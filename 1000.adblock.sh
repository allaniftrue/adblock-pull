#!/bin/sh

echo    "" > /home/freebsd/scripts/adblock.conf
while read line
do
	if [ ! -z "$line" ]
	then
		echo    "local-zone: \"$line\" redirect" >> /home/freebsd/scripts/adblock.conf
       		echo    "local-data: \"$line A 178.62.249.73\"" >> /home/freebsd/scripts/adblock.conf
	fi
done < /home/freebsd/scripts/filter_compiled.txt

/usr/sbin/unbound-control dump_cache > /home/freebsd/scripts/dump.txt
kill -HUP `cat /var/run/local_unbound.pid`
/usr/sbin/service local_unbound start
/usr/sbin/unbound-control load_cache < /home/freebsd/scripts/dump.txt

now=$(date +"%T")
echo "cron job exec : $now" >> /home/freebsd/scripts/cron.log
