#!/usr/local/bin/php

<?php

$domain_stack = Array();
$filtered_stack = Array();

# Get contents from easylist domain
$content = file_get_contents('https://easylist-downloads.adblockplus.org/easylist.txt');

# Extract adserver list only
preg_match('/(\!\s\*\*\*\seasylist:easylist\/easylist_adservers\.txt\s\*\*\*)[^"]+(\!(\-)+Third\-party\sadverts(\-)+\!)/', $content, $extracted, PREG_OFFSET_CAPTURE);

$content = preg_replace('/\!(\-)+(.*?)(\-)+\!/', '', $extracted[0][0]);

$content = str_replace('||', '', $content);
$individual_lines = explode("\n", $content);

array_walk_recursive($individual_lines, function ($item, $key) use (&$domain_stack) {
    $item = trim($item);
    if(strpos($item, '~') === false && isset($item)) {
        $item = preg_replace('/\^(.*?).*/', '', $item);
        $item = preg_replace('/\$(.*?).*/', '', $item);
        $item = preg_replace('/third\-party/', '', $item);
        $item = preg_replace(('/^\!\s(.*?).*/'), '', $item);
    }
    array_push($domain_stack, $item);
});

$glue_domain_stack = implode("\n", $domain_stack);
$explode_variants = explode(",", $glue_domain_stack);

$glue_filtered_domain_stack = implode("\n", $explode_variants);
$explode_filtered_variants = explode("|", $glue_filtered_domain_stack);

$glue_exploded_variants = implode("\n", $explode_filtered_variants);

$individual_filtered_domains = explode("\n", $glue_exploded_variants);


array_walk_recursive($individual_filtered_domains, function ($item, $key) use (&$filtered_stack) {

    $item = preg_replace('/\^(.*?).*/', '', $item);
    $item = preg_replace('/\$(.*?).*/', '', $item);
    $item = preg_replace('/third\-party/', '', $item);
    $item = preg_replace(('/^\!\s(.*?).*/'), '', $item);
    $item = preg_replace('/domain\=\~/', '', $item);
    $item = preg_replace('/\~/', '', $item);
    $item = preg_replace('/[*]?\.ip/', "", $item);
    $item = trim($item);

    if(isset($item)) {
        $url = 'http://'.$item;
        if(filter_var($url, FILTER_VALIDATE_URL) || filter_var($url, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4|FILTER_FLAG_NO_PRIV_RANGE)) {

            $domain = parse_url($url);
            array_push($filtered_stack, $domain['host']);

        }
    }

});

$unique_domains = array_unique($filtered_stack);
$flatten_unique_domains = implode("\n", $unique_domains);

file_put_contents('easylist_domains.txt', $flatten_unique_domains);
?>

