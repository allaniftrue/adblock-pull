#!/usr/local/bin/php

<?php
    $content = file_get_contents("compiled.txt");
    $individual_lines = explode("\n", $content);
    $unique_domains = array_unique($individual_lines);
    $flatten_unique_domains = implode("\n", $unique_domains);
    file_put_contents('filter_compiled.txt', $flatten_unique_domains);
?>

