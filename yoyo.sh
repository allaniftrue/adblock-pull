#!/bin/sh
#
# Convert the Yoyo.org anti-ad server listing
# into an unbound dns spoof redirection list.

wget -q -O- --header\="Accept-Encoding: gzip" 'http://pgl.yoyo.org/adservers/serverlist.php?hostformat=hosts&mimetype=plaintext' | \
gunzip | \
awk '/^127\./{
        print $2 
}' > /usr/home/freebsd/scripts/unbound_ad_servers.txt
